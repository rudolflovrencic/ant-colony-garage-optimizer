#include "Lane.h"

#include <stdlib.h>

void free_lane(Lane* l)
{ 
    /* Check if lane has blocked data and free if it has. */
    if (l->blocked != NULL && l->n_blocked > 0) { /* Single check is enough. */
        free(l->blocked);
    }
}
