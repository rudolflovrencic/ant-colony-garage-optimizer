#include "Errors.h"

#include <stdio.h>
#include <stdlib.h>


void file_opening_error(const char* filename, enum Operation op)
{
    fprintf(stderr,
        "Could not open file \"%s\" for %s.\n"
        "Now exiting...\n",
        filename,
        (op == READING) ? "reading" : "writing");
    exit(1);
}


void xml_file_reading_error(const char* filename)
{
    fprintf(stderr,
        "Failed to extract XML document data from file \"%s\".\n"
        "Now exiting...\n",
        filename);
    exit(1);
}


void xml_missing_parameters_error(enum ManditoryParameters parameter)
{
    switch (parameter) {
        case INSTANCE_FILENAME:
            fprintf(stderr,
                "No instance file provided in the XML parameters file.\n");
            break;
        case TERMINATION_CONDITION:
            fprintf(stderr,
                "Not a single termination condition was provided in the XML"
                " parameters file.\n");
            break;
        default:
            fprintf(stderr, "XML parameters file error.\n");
    }
    fprintf(stderr, "Now exiting...\n");
    exit(1);
}
