#include "Sorter.h"

static void swap(int *x,int *y)
{
    int temp = *x;
    *x = *y;
    *y = temp;
}


static int choose_pivot(int i,int j )
{
    return (i + j) / 2;
}


void quicksort(int* vil, int m, int n, const Instance* inst)
{
    int key, i, j, k;
    if (m < n) {
        k = choose_pivot(m, n);
        swap(&vil[m], &vil[k]);
        key = inst->vehicles[vil[m]].departure;
        i = m + 1;
        j = n;
        while (i <= j) {
            while ((i <= n) && (inst->vehicles[vil[i]].departure <= key)) i++;
            while ((j >= m) && (inst->vehicles[vil[j]].departure >  key)) j--;
            if (i < j) swap(&vil[i], &vil[j]);
        }
        /* Swap two elements. */
        swap(&vil[m], &vil[j]);
 
        /* Recursively sort the lesser list. */
        quicksort(vil, m, j - 1, inst);
        quicksort(vil, j + 1, n, inst);
    }
}
