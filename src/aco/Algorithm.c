#include "aco/Algorithm.h"

#include "aco/Termination.h"
#include "aco/Filter.h"

#include <string.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>


static void init_algorithm_calculation_area(State* s, const Instance* inst);
static void free_algorithm_calculation_area(State *s);
static int roulette_wheel(const double* probs, int n_probs);
static void init_etas(State *s, const Instance* inst);
static void free_etas(State *s);
static int pick_value(int vi, const State* s, const Parameters* pars,
        const Instance* inst, const Solution* sol);


void start_aco_algorithm(const Instance* inst, const Parameters* pars, int* bs)
{
    State s; /* Algorithm state. */

    /* Calculate initial pheromone value and initialize pheromones. */
    double ipv = 1.0 / (inst->n_vehicles * inst->n_lanes);
    init_pheromones(&s.phr, inst->n_vehicles * inst->n_lanes, ipv);

    /* Initialize the population (individual contains n_vehicles integers). */
    init_population(&s.p, pars->population_size, inst->n_vehicles);

    init_etas(&s, inst); /* Allocate memory and calculate eta values. */

    /* Pre-allocate calculation area. */
    init_algorithm_calculation_area(&s, inst);
    init_fitness_calculation_area(inst);

    s.start_time = time(NULL); /* Set starting time (in seconds). */

    /* Run the algorithm until one of the termination criteria is met. */
    for (s.iteration = 0; !termination_criteria_met(&s, pars); s.iteration++) {
        
        /* For each ant (solution) in the population. */
        for (int si = 0; si < s.p.pop_size; si++) {
            Solution *sol = &s.p.sols[si]; /* Pointer to 'si'-th solution. */

            /* Reset solution to zero. */
            for (int i = 0; i < s.p.sol_size; i++) {
                sol->sol_data[i] = -1;
            }

            /* For each variable (dimension) in the solution. */
            for (int vi = 0; vi < s.p.sol_size; vi++) {
                sol->sol_data[vi] = pick_value(vi, &s, pars, inst, sol);
            }

            /* Evaluate constructed solution and remember it if it is best. */
            sol->fitness = evaluate(sol, inst);
            if (s.bs.fitness <= sol->fitness) {
                memcpy(s.bs.sol_data, sol->sol_data,
                        inst->n_vehicles * sizeof(int));
                s.bs.fitness = sol->fitness;
            }
        }
        /* Update pheromone trails. */
        evaporate_pheromones(&s.phr, pars->rho);
        reinforce_pheromones(&s.phr, &s.bs, inst->n_vehicles, inst->n_lanes);
        
        /* Logging. */
        if (s.iteration % pars->logging_frequency == 0) {
            printf("%2d (%2lds): ", s.iteration, time(NULL) - s.start_time);
            printf("[%f]  =>  ", s.bs.fitness); 
            for (int i = 0; i < inst->n_vehicles; i++) {
                printf("%2d ", s.bs.sol_data[i]);
            }
            printf("\n"); 
        }
    }
    
    /* Copy best solution data into return array. */
    memcpy(bs, s.bs.sol_data, inst->n_vehicles * sizeof(int));
    
    free_fitness_calculation_area();
    free_algorithm_calculation_area(&s); /* Free memory for calculations. */
    free_etas(&s);                       /* Free eta array. */
    free_population(&s.p);               /* Free population data. */
    free_pheromones(&s.phr);             /* Free pheromone data. */
}


/* Returns random index smaller than n_probs with probability of each index
 * provided in probs array. */
static int roulette_wheel(const double* probs, int n_probs)
{
   double gen = (double)rand() / RAND_MAX;     
   
   double sum = 0;
   for (int i = 0; i < n_probs; i++) {
       sum += probs[i];
       if (sum >= gen) return i;
   }
   return n_probs - 1; /* Only should happen due to numerical errors. */
}


/* Pointer to heap allocated array that holds probabilities of picking each
 * individual solution component. */
static double *probs;

/* Picks a value for solution variable (dimension) at index 'vi' based on
 * pheromone values and eta values. In other words, picks a lane. */
static int pick_value(int vi, const State* s, const Parameters* pars,
        const Instance* inst, const Solution* sol)
{
    /* Calculate probability denominator. */
    double denominator = 0.0;
    for (int li = 0; li < inst->n_lanes; li++) {
        double phr   = s->phr.phr_data[vi * inst->n_lanes + li];
        double eta   = s->etas[vi * inst->n_lanes + li];
        denominator += pow(phr, pars->alpha) * pow(eta, pars->beta);
    } 

    /* Calculate probabilities using the denominator. */
    for (int li = 0; li < inst->n_lanes; li++) {
        double phr = s->phr.phr_data[vi * inst->n_lanes + li];
        double eta = s->etas[vi * inst->n_lanes + li];
        probs[li]  = pow(phr, pars->alpha) * pow(eta, pars->beta) / denominator;
    }

    /* Filter out variables that produce unfeasible solutions. */
    filter_probabilities(probs, inst, sol, vi); 

    /* Weighted pick random lane index and return it. */
    return roulette_wheel(probs, inst->n_lanes);
}


/* Has to be called before starting the main algorithm loop. */
static void init_algorithm_calculation_area(State *s, const Instance* inst)
{
    probs = (double*) malloc(inst->n_lanes * sizeof(double)); 

    /* Initialize best found solution. */
    s->bs.sol_data = (int*) malloc(inst->n_vehicles * sizeof(int));
    s->bs.fitness = -INFINITY;
}


static void free_algorithm_calculation_area(State *s)
{
    free(probs);
    free(s->bs.sol_data);
}


static void init_etas(State *s, const Instance* inst)
{
    /* Allocate space for eta values. */
    s->etas = (double*) malloc(inst->n_vehicles
            * inst->n_lanes * sizeof(double));
    
    for (int vi = 0; vi < inst->n_vehicles; vi++) {
        /* Pointer to start of constraint data for vehicle with index vi. */
        const int* cfv = inst->c_matrix + vi * inst->n_lanes;   
        
        int nal = 0; /* Number of available lanes for vehicle. */
        for (int li = 0; li < inst->n_lanes; li++)  nal += cfv[li]; 
        
        for (int li = 0; li < inst->n_lanes; li++) {
            double eta_ij = (double) cfv[li] / nal; 
            s->etas[vi * inst->n_lanes + li] = eta_ij;
        }
    }
}


static void free_etas(State *s)
{
    free(s->etas);
}
