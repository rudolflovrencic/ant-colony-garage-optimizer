#include "aco/Pheromones.h"

#include <malloc.h>


void init_pheromones(Pheromones* phr, int n_pheromones, double iv)
{
    phr->n_pheromones = n_pheromones; 
   
    /* Allocate memory for pheromone trails. */
    phr->phr_data = (double*) malloc(n_pheromones * sizeof(double));

    /* Set all pheromones to initial value. */
    for (int i = 0; i < n_pheromones; i++) {
        phr->phr_data[i] = iv;
    }
    
}


void free_pheromones(Pheromones* phr)
{
    free(phr->phr_data);
}


void evaporate_pheromones(Pheromones* phr, double rho)
{
    for (int i = 0; i < phr->n_pheromones; i++) {
        phr->phr_data[i] *= 1 - rho; 
    }
}


void reinforce_pheromones(Pheromones* phr, const Solution* bf, int sol_size,
        int n_lanes)
{
    for (int vi = 0; vi < sol_size; vi++) {
        int li = bf->sol_data[vi];
        /*phr->phr_data[vi * n_lanes + li] += bf->fitness;*/
        phr->phr_data[vi * n_lanes + li] += 0.001; /* Fixed reward. */
        if (phr->phr_data[vi * n_lanes + li] > 1.0) { /* Cap pheromones. */
            phr->phr_data[vi * n_lanes + li] = 1.0;
        }
    }
}
