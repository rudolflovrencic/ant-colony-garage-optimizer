#include "Instance.h"

#include <stdlib.h>
#include <stdio.h>

#include "Errors.h"

static void load_vehicle_lengths_and_series(FILE* f, Instance* x);
static void load_constraint_matrix(FILE* f, Instance* x);
static void load_lane_lengths(FILE* f, Instance* x);
static void load_departure_times_and_schedule_types(FILE* f, Instance* x);
static void load_blocking_data(FILE* f, Instance* x);
static char consume_spaces_untill_end_of_line(FILE* f);


void load_instance_data(Instance* x, const char* filename)
{
    FILE* f;
    f = fopen(filename, "r");
     
    if (f == NULL) {
        file_opening_error(filename, READING);
    } else {
        /* Load number of vehicles and number of lanes. */
        fscanf(f, "%d%d", &x->n_vehicles, &x->n_lanes); 

        /* Load vehicle lengths and series. */
        load_vehicle_lengths_and_series(f, x);
        
        /* Load constraint matrix. */
        load_constraint_matrix(f, x);

        /* Load lane lengths. */
        load_lane_lengths(f, x);

        /* Load departure times and schedule types. */
        load_departure_times_and_schedule_types(f, x);

        /* Load lane blocking data. */
        load_blocking_data(f, x);

        fclose (f);
    }
}


void free_instance_data(Instance* x)
{
    free(x->vehicles); /* Vehicles contain no heap data. */

    /* Free heap data of each lane and then free lanes array. */
    for (int i = 0; i < x->n_lanes; i++) {
        free_lane(&x->lanes[i]); 
    }
    free(x->lanes);

    /* Free heap data for constraint matrix. */
    free(x->c_matrix);
}


static void load_vehicle_lengths_and_series(FILE* f, Instance* x)
{
    /* Allocate memory for vehicles. */
    x->vehicles = (Vehicle*) malloc(x->n_vehicles * sizeof(Vehicle));

    /* Set IDs and read length of each vehicle. */
    for (int i = 0; i < x->n_vehicles; i++) {
        x->vehicles[i].ID = i + 1; /* Set ID (index with offset of 1). */
        fscanf(f, "%d", &x->vehicles[i].length); /* Read length from file. */
    }
    
    /* Read series of each vehicle. */
    for (int i = 0; i < x->n_vehicles; i++) {
        fscanf(f, "%d", &x->vehicles[i].series); /* Read series from file. */
    }
}


static void load_constraint_matrix(FILE* f, Instance* x)
{
    /* Allocate memory for constraint matrix and load data from the file. */
    x->c_matrix = (int*) malloc(x->n_vehicles * x->n_lanes * sizeof(int));
    for (int i = 0; i < x->n_vehicles * x->n_lanes; i++) {
        fscanf(f, "%d", x->c_matrix + i);
    }
}


static void load_lane_lengths(FILE* f, Instance* x)
{ 
    /* Allocate memory for lanes. */
    x->lanes = (Lane*) malloc(x->n_lanes * sizeof(Lane));

    /* Set IDs and load lane lengths from the file. */
    for (int i = 0; i < x->n_lanes; i++) {
        x->lanes[i].ID = i + 1; /* Set ID (index with offset of 1). */
        fscanf(f, "%d", &x->lanes[i].length);
    }
}


static void load_departure_times_and_schedule_types(FILE* f, Instance* x)
{
    /* Load vehicle departure times from the file. */
    for (int i = 0; i < x->n_vehicles; i++) {
        fscanf(f, "%d", &x->vehicles[i].departure);
    }
    
    /* Load vehicle schedule types from the file. */
    for (int i = 0; i < x->n_vehicles; i++) {
        fscanf(f, "%d", &x->vehicles[i].schedule);
    }
}


static void load_blocking_data(FILE* f, Instance* x)
{
    /* Initialize number of blocked lanes and blocked pointer. */
    for (int i = 0; i < x->n_lanes; i++) {
        x->lanes[i].n_blocked = 0;
        x->lanes[i].blocked = NULL;
    }

    int blocking_lane, blocked_lane, loaded;
    char c;
    do {
        loaded = fscanf(f, "%d%d", &blocking_lane, &blocked_lane);
        if (loaded != 2) break; /* Check if no constraint data is given. */
        
        /* Fetch blocking lane pointer (shortcut) and set first blocked lane. */
        Lane* l = &x->lanes[blocking_lane - 1]; /* Index in decremented ID. */
        l->n_blocked = 1; /* Only one blocked lane is found for now. */
        l->blocked = (int*) malloc(sizeof(int)); 
        l->blocked[0] = blocked_lane;

        c = consume_spaces_untill_end_of_line(f);
        while (c != '\n' && c != EOF) { /* Load other blocked lanes. */
            loaded = fscanf(f, "%d", &blocked_lane); 

            /* Found new blocked lane - make room for it and save it. */
            l->n_blocked++;
            l->blocked = (int*) realloc(l->blocked, l->n_blocked * sizeof(int));
            l->blocked[l->n_blocked - 1] = blocked_lane;
            
            c = consume_spaces_untill_end_of_line(f);
        } 
    } while(c != EOF);
}

static char consume_spaces_untill_end_of_line(FILE* f) {
    char c;
    do {
        c = getc(f); 
    } while ((c == ' ' || c == '\t') && c != EOF); 
    if (c != '\n' && c != EOF) { /* End of line has not been reached. */
        ungetc(c, f);            /* Return consumed character to the stream. */
    }
    return c;
}
