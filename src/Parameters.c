#include "Parameters.h"

#include <libxml/parser.h>
#include <string.h>
#include <math.h>

#include "Errors.h"


static void walk_tree(xmlNode* n, Parameters* p);
static void process_node(xmlNode* n, Parameters* p);
static void set_default_parameter_values(Parameters* p);
static void validate_parameters(const Parameters* p);


void load_parameters(Parameters* p, const char* filename)
{
    /* Initialize parameters structure. */
    set_default_parameter_values(p);

    /* Read the XML document and check if it was successfully read. */
    xmlDoc* document = xmlReadFile(filename, NULL, 0);
    if (document == NULL) {
        xml_file_reading_error(filename);
    }

    /* Fetch the root of the XML document tree. */
    xmlNode* root = xmlDocGetRootElement(document);

    /* Recursively walk XML document tree and fill parameters structure. */
    walk_tree(root, p);

    /* Clean up XML parser allocated memory. */
    xmlFreeDoc(document);
    xmlCleanupParser();

    /* Check if all manditory parameters are properly set. */
    validate_parameters(p);
}


static void validate_parameters(const Parameters* p)
{
    if (p->instance_filename == NULL) { /* No instance filename was provided. */
        xml_missing_parameters_error(INSTANCE_FILENAME);
    } else if (p->max_iter <= 0                 /* No max iterations. */ 
            && p->max_time <= 0                 /* No max time. */
            && p->target_fitness == INFINITY) { /* No target fitness. */
        xml_missing_parameters_error(TERMINATION_CONDITION);
    } 
    
}


void free_parameters_data(Parameters* p)
{
    free(p->instance_filename);
}


static void set_default_parameter_values(Parameters* p)
{
    p->instance_filename = NULL;
    p->max_iter          = 0;
    p->max_time          = 0;
    p->target_fitness    = INFINITY;
    p->logging_frequency = 10;
}


static void walk_tree(xmlNode* node, Parameters* p)
{
    for (xmlNode* n = node; n; n = n->next) {
        process_node(n, p); /* Process current node. */
        walk_tree(n->children, p); /* Walk children of current node. */
    } 
} 


static void process_node(xmlNode* n, Parameters* p)
{
    /* If node contains data, determine parent node name to interpret data. */ 
    if (n->type == XML_TEXT_NODE) { /* XML text node (text data). */
        if (!strcmp((const char*)n->parent->name, "instance_filename")) {
            p->instance_filename =
                (char*) malloc(strlen((const char*)n->content) + 1);
            strcpy(p->instance_filename, (const char*)n->content);
        } else if (!strcmp((const char*)n->parent->name, "max_iter")) {
            p->max_iter = atoi((const char*)n->content);
        } else if (!strcmp((const char*)n->parent->name, "population_size")) {
            p->population_size = atoi((const char*)n->content);
        } else if (!strcmp((const char*)n->parent->name, "alpha")) {
            p->alpha = atof((const char*)n->content);
        } else if (!strcmp((const char*)n->parent->name, "beta")) {
            p->beta = atof((const char*)n->content);
        } else if (!strcmp((const char*)n->parent->name, "rho")) {
            p->rho = atof((const char*)n->content);
        } else if (!strcmp((const char*)n->parent->name, "max_time")) {
            p->max_time = atoi((const char*)n->content);
        } else if (!strcmp((const char*)n->parent->name, "target_fitness")) {
            p->target_fitness = atof((const char*)n->content);
        } else if (!strcmp((const char*)n->parent->name, "logging_frequency")) {
            p->logging_frequency = atoi((const char*)n->content);
        }
    }
}

