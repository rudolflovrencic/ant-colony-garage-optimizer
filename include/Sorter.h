#ifndef SORTER_H
#define SORTER_H

#include "Instance.h"

void quicksort(int* vil, int m, int n, const Instance* inst);

#endif
