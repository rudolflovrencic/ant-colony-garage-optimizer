#ifndef PARAMTERS_H
#define PARAMTERS_H

typedef struct {
    char* instance_filename;
    int max_iter;
    int max_time;
    int population_size;
    double alpha;
    double beta;
    double rho;
    double target_fitness;
    int logging_frequency;
} Parameters;

void load_parameters(Parameters* p, const char* filename);
void free_parameters_data(Parameters* p);

#endif
