#ifndef ERRORS_H
#define ERRORS_H

enum Operation {
    READING,
    WRITING
};
void file_opening_error(const char* filename, enum Operation op);

void xml_file_reading_error(const char* filename);

enum ManditoryParameters {
    INSTANCE_FILENAME,
    TERMINATION_CONDITION
};
void xml_missing_parameters_error(enum ManditoryParameters parameter);


#endif
