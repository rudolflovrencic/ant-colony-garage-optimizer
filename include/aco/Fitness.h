#ifndef FITNESS_H
#define FITNESS_H

#include "aco/Population.h"

#include "Instance.h"

/* Returns fitness of provided solution. */
double evaluate(const Solution* s, const Instance* inst);

/* Allocates heap space for fitness calculation. This function has to be called
 * before any fitness calculation takes place. */
void init_fitness_calculation_area(const Instance* inst);

/* Frees heap allocated space that was used for fitness calculation. After this
 * function has been called, no further fitness calculations is allowed. */
void free_fitness_calculation_area();

/* Returns number of co-residing lanes that differ in vehicle series that they
 * host. Note that this function only checks the first vehicle in the lane and
 * assumes all other vehicles in the lane are of the same series. Empty lanes
 * are skipped. The function uses decoded solution global variable to perform
 * calculations. */
int f1(const Instance* inst);

/* Returns number of used lanes, in the provided solution. Does not use global
 * variable that stores decoded solution to perform calculations. */
int f2(const Solution* s, int sol_size);

/* Returns sum of available space in used lanes. The function uses decoded
 * solution global variable to perform calculations. */
double f3(const Instance* inst);

/* Returns number of co-residing vehicles in the lane that have the same
 * schedule type. Returns total sum among all lanes. The function uses decoded
 * solution global variable to perform calculations. */
int g1(const Instance* inst);

/* Returns number of co-residing tracks in which the last vehicle in the first
 * first track has the same schedule type as the first vehicle in the second
 * track. Empty lanes are skipped. The function uses decoded solution global
 * variable to perform calculations. */
int g2(const Instance* inst);

/* Returns sum of rewards and punishments calculated based on times of departure
 * of co-residing vehicles. The function uses decoded solution global variable
 * to perform calculations. Function also returns number of evaluated vehicle
 * pairs via the variable passed in. */
int g3(const Instance* inst, int* n_evaluated_pairs);

/* Decodes the solution by storing it into the matrix [n_lanes x n_vehicles]
 * where each row holds indices of vehicles in the lane with index equal to row
 * index. Vehicles in each row are sorted by departure time (vehicle in the
 * lowest column index has the smallest departure time). */
void decode_solution(const Solution* s, const Instance* inst);

/* Holds decoded solution. It is a matrix the size of [n_lanes x n_vehicles]
 * where each row holds indices of vehicles in the lane with index equal to row
 * index. Vehicles in each row are sorted by departure time (vehicle in the
 * lowest column index has the smallest departure time). Negative vehicle index
 * means that given slot in the lane is not taken. */
int* decoded_solution;

/* Holds the number of vehicles in each lane. */
int* nvpl;

/* Sum of capacities of all lanes. */
int total_lane_capacity;

/* Sum of lengths of all vehicles. */
int total_vehicle_length;


#endif
