#ifndef PHEROMONE_H
#define PHEROMONE_H

#include "Population.h"

typedef struct {
    double* phr_data;
    int n_pheromones;
} Pheromones;

void init_pheromones(Pheromones* phr, int n_pheromones, double iv);
void free_pheromones(Pheromones* phr);

void evaporate_pheromones(Pheromones* phr, double rho);
void reinforce_pheromones(Pheromones* phr, const Solution* bf, int sol_size,
        int n_lanes);

#endif
