#ifndef FILTER_H
#define FILTER_H

#include <stdbool.h>

#include "Instance.h"
#include "Population.h"

/* Filters out probabilities for choices of variable at index 'vi' that would
 * cause solution to be unfeasible. The function has to first decode solution
 * to determine lanes in which the vehicle can be placed. To determine that,
 * the function has to filter out full lanes and lanes that contain vehicles
 * with different series that vehicle at index 'vi' (currently considered
 * vehicle). Note that the function does not need to consider constraint matrix
 * since eta values make sure that probabilities are zero where constraint
 * matrix denied lane for currently considered vehicle. */
void filter_probabilities(double* probs, const Instance* inst,
        const Solution* sol, int vi);


/* Check if solution is unfeasible by checking three hard constraints:
 * 1) Vehicles in each lane can actually fit in their lane.
 * 2) Vehicles in blocking lanes do not block any vehicles in blocked lanes. 
 * 3) Check if each lane holds only one vehicle series.
 * Note that this function checks agains decoded solution in the global
 * variable. */
bool solution_is_valid(const Instance* inst);


/* Returns false if any of vehicles in blocking lanes block any vehicles in
 * blocked lanes. This means that vehicle with highest departure time (last
 * vehicle) in any of the blocking lanes has higher departure time then first
 * vehicle in each of the blocked lanes. Note that this function uses decoded
 * solution from the global variable. */
bool valid_lane_blocking(const Instance* inst);

/* Returns false if any of the lanes hosts vehicles with different series. Note
 * that this function uses decoded solution from the global variable. */
bool valid_lane_series(const Instance* inst);

#endif
