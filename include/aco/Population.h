#ifndef POPULATION_H
#define POPULATION_H

typedef struct {
    int* sol_data;  /* Pointer to segment of population data. Not owner. */
    double fitness; /* Fitnes of solution. -INFINITY if not calculated yet. */
} Solution;

typedef struct {
    int* pop_data;  /* Allocated data for all solutions. */
    int pop_size;   /* Number of solutions (ants) in the population. */
    int sol_size;   /* Number of integers in single solution. */
    Solution* sols; /* Holds pop_size solutions. */
} Population;

void init_population(Population* p, int pop_size, int sol_size);
void free_population(Population* p);

#endif
