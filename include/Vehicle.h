#ifndef VEHICLE_H
#define VEHICLE_H

typedef struct {
    int ID;
    int length;
    int series;
    int departure;
    int schedule;
} Vehicle;

#endif
