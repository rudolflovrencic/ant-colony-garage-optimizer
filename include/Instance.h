#ifndef INSTANCE_H
#define INSTANCE_H

#include "Vehicle.h"
#include "Lane.h"

typedef struct {
    int n_vehicles;
    int n_lanes;
    Vehicle* vehicles;
    Lane* lanes;
    int* c_matrix; /* Constraint matrix. [n_vehicles x n_lanes] */
} Instance;

void load_instance_data(Instance* x, const char* filename);
void free_instance_data(Instance* x);

#endif
