#ifndef TESTS_H
#define TESTS_H

#include <check.h>

Suite* parameters_loading_suite();
Suite* population_init_suite();
Suite* fitness_calculation_suite();
Suite* sorting_suite();
Suite* filter_suite();

Suite* instance_loading_suite();
void free_testing_instace();

#endif
