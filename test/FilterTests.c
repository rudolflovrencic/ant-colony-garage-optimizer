#include "Tests.h"

#include "aco/Filter.h"

#include "aco/Population.h"
#include "aco/Fitness.h"

START_TEST (lane_series_test)
{
    Population pop; 
    init_population(&pop, 1, 5);

    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");

    init_fitness_calculation_area(&x);

    /* Initialize first solution: [1, 1, 0, 3, 3}. */
    pop.sols[0].sol_data[0] = 1; /* Series: 2 */
    pop.sols[0].sol_data[1] = 1; /* Series: 2 */
    pop.sols[0].sol_data[2] = 0; /* Series: 1 */
    pop.sols[0].sol_data[3] = 3; /* Series: 1 */
    pop.sols[0].sol_data[4] = 3; /* Series: 1 */
    
    decode_solution(&pop.sols[0], &x);
    ck_assert(valid_lane_series(&x));
    
    pop.sols[0].sol_data[3] = 1;
    decode_solution(&pop.sols[0], &x);
    ck_assert(!valid_lane_series(&x));

    /* Free fitness calculation area and population data. */
    free_fitness_calculation_area();
    free_population(&pop);
    free_instance_data(&x);
}
END_TEST


START_TEST (lane_blocking_test)
{
    Population pop; 
    init_population(&pop, 1, 5);

    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");

    init_fitness_calculation_area(&x);

    pop.sols[0].sol_data[0] = 2; /* Departure: 288 */
    pop.sols[0].sol_data[1] = 1; /* Departure: 333 */
    pop.sols[0].sol_data[2] = 3; /* Departure: 345 */
    pop.sols[0].sol_data[3] = 3; /* Departure: 360 */
    pop.sols[0].sol_data[4] = 0; /* Departure: 276 */
    
    decode_solution(&pop.sols[0], &x);
    ck_assert(valid_lane_blocking(&x));

    /* Free fitness calculation area and population data. */
    free_fitness_calculation_area();
    free_population(&pop);
    free_instance_data(&x);
}
END_TEST


START_TEST (large_lane_blocking_test)
{

    Instance x;
    load_instance_data(&x, "res/instance2.txt");
    
    Population pop; 
    init_population(&pop, 1, x.n_vehicles);

    int lanes[] = {14,8,13,27,5,12,1,11,18,18,11,20,4,28,17,2,19,6,16,21,19,17,22,16,15,20,8,15,7,8,10,5,25,9,25,5,9,2,26,10,14,13,10,7,23,7,3,23,21,0,3,6,22};
    memcpy(pop.pop_data, lanes, sizeof(lanes));

    init_fitness_calculation_area(&x);
    decode_solution(&pop.sols[0], &x);
    ck_assert(valid_lane_blocking(&x));

    /* Free fitness calculation area and population data. */
    free_fitness_calculation_area();
    free_population(&pop);
    free_instance_data(&x);
}
END_TEST



Suite* filter_suite()
{
    Suite* s = suite_create("Filtering suite");
    TCase* tc_core = tcase_create("Core");

    tcase_add_test(tc_core, lane_blocking_test);
    tcase_add_test(tc_core, large_lane_blocking_test);
    tcase_add_test(tc_core, lane_series_test);

    suite_add_tcase(s, tc_core);
    return s;
}
