#include "Tests.h"

#include "aco/Population.h"

#include <stdio.h>

/* Check if population is viewed correctly through solutions. */
START_TEST (population_view_through_solutions)
{
    Population pop; 
    init_population(&pop, 4, 3);
    for (int i = 0; i < pop.pop_size * pop.sol_size; i++) {
        pop.pop_data[i] = i + 1;
    }

    /* Check first solution values. */
    ck_assert_int_eq(pop.sols[0].sol_data[0], 1);
    ck_assert_int_eq(pop.sols[0].sol_data[1], 2);
    ck_assert_int_eq(pop.sols[0].sol_data[2], 3);
    
    /* Check second solution values. */
    ck_assert_int_eq(pop.sols[1].sol_data[0], 4);
    ck_assert_int_eq(pop.sols[1].sol_data[1], 5);
    ck_assert_int_eq(pop.sols[1].sol_data[2], 6);
 
    /* Check third solution values. */
    ck_assert_int_eq(pop.sols[2].sol_data[0], 7);
    ck_assert_int_eq(pop.sols[2].sol_data[1], 8);
    ck_assert_int_eq(pop.sols[2].sol_data[2], 9);

    /* Check fourth solution values. */
    ck_assert_int_eq(pop.sols[3].sol_data[0], 10);
    ck_assert_int_eq(pop.sols[3].sol_data[1], 11);
    ck_assert_int_eq(pop.sols[3].sol_data[2], 12);

    free_population(&pop);
}
END_TEST


Suite* population_init_suite()
{
    Suite* s = suite_create("Population initialization suite.");
    TCase* tc_core = tcase_create("Core");

    tcase_add_test(tc_core, population_view_through_solutions);

    suite_add_tcase(s, tc_core);
    return s;
}
