#include "Tests.h"

#include "Instance.h"

Instance x;

/* Check if number of vehicles loaded correctly. */
START_TEST (number_of_vehicles)
{
    ck_assert_int_eq(x.n_vehicles, 5);
}
END_TEST

/* Check if number of lanes loaded correctly. */
START_TEST (number_of_lanes)
{
    ck_assert_int_eq(x.n_lanes, 4);
}
END_TEST

/* Check if vehicle lengths are loaded correctly. */
START_TEST (vehicle_lengths)
{
    int expected_lengths[5] = {42, 48, 42, 53, 37};
    for (int i = 0; i < 5; i++) {
        ck_assert_int_eq(x.vehicles[i].length, expected_lengths[i]);
    }
}
END_TEST

/* Check if constraint matrix is loaded correctly. */
START_TEST (constraint_matrix)
{
    int m[5][4] = {{1, 0, 1, 1},
                   {1, 1, 1, 1},
                   {1, 0, 0, 1},
                   {1, 1, 1, 1},
                   {0, 1, 0, 1}};

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 4; j++) {
            ck_assert_int_eq(x.c_matrix[i * x.n_lanes + j], m[i][j]);
        }
    }
}
END_TEST

/* Check if lane lengths are loaded correctly. */
START_TEST (lane_lengths)
{
    int expected_lengths[4] = {140, 182, 123, 144};

    for (int i = 0; i < 4; i++) {
        ck_assert_int_eq(x.lanes[i].length, expected_lengths[i]);
    }
}
END_TEST

/* Check if vehicle departure times are loaded correctly. */
START_TEST (departure_times)
{
    int expected_departures[5] = {288, 333, 345, 360, 276};

    for (int i = 0; i < 5; i++) {
        ck_assert_int_eq(x.vehicles[i].departure, expected_departures[i]);
    }
}
END_TEST

/* Check if schedule types are loaded correctly. */
START_TEST (schedule_types)
{
    int expected_schedules[5] = {1, 4, 0, 6, 6};

    for (int i = 0; i < 5; i++) {
        ck_assert_int_eq(x.vehicles[i].schedule, expected_schedules[i]);
    }
}
END_TEST

/* Check if blocking constraints are loaded correctly. */
START_TEST (blocking_constraints)
{
    
    {   /* Multiple elements in the blocked list. */
        int expected_blocking[3] = {2, 3, 4};
        for (int i = 0; i < 3; i++) {
            ck_assert_int_eq(x.lanes[1 - 1].blocked[i], expected_blocking[i]);
        }
    }
    {   /* Single element in the blocked list. */
        ck_assert_int_eq(x.lanes[3 - 1].blocked[0], 2);
    }
}
END_TEST


Suite* instance_loading_suite()
{
    Suite* s = suite_create("Instance loading suite");
    TCase* tc_core = tcase_create("Core");

    load_instance_data(&x, "test/res/test_instance.txt");

    tcase_add_test(tc_core, number_of_vehicles);
    tcase_add_test(tc_core, number_of_lanes);
    tcase_add_test(tc_core, vehicle_lengths);
    tcase_add_test(tc_core, constraint_matrix);
    tcase_add_test(tc_core, lane_lengths);
    tcase_add_test(tc_core, departure_times);
    tcase_add_test(tc_core, schedule_types);
    tcase_add_test(tc_core, blocking_constraints);

    suite_add_tcase(s, tc_core);
    return s;
}


void free_testing_instace() 
{
    free_instance_data(&x);
}


