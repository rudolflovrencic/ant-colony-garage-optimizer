#include "Tests.h"

#include "Parameters.h"

#include <math.h>

#define F_EQ 1e-6

/* Check if XML parameters data is loaded properly. */
START_TEST (xml_parameter_loading)
{
    Parameters p;
    load_parameters(&p, "test/res/test_parameters.xml");

    ck_assert_str_eq(p.instance_filename, "res/instance.txt");
    ck_assert_int_eq(p.population_size, 100);
    ck_assert(fabs(p.alpha - 1.2) < F_EQ);
    ck_assert(fabs(p.beta - 1.8) < F_EQ);
    ck_assert(fabs(p.alpha - 1.2) < F_EQ);
    ck_assert(fabs(p.beta - 1.8) < F_EQ);
    ck_assert(fabs(p.rho - 0.05) < F_EQ);
    ck_assert(fabs(p.rho - 0.05) < F_EQ);
    ck_assert_int_eq(p.max_iter, 10000);
    ck_assert_int_eq(p.max_time, 300);
    ck_assert(fabs(p.target_fitness - 421.22) < F_EQ);
}
END_TEST


Suite* parameters_loading_suite()
{
    Suite* s = suite_create("Parameters loading suite");
    TCase* tc_core = tcase_create("Core");

    tcase_add_test(tc_core, xml_parameter_loading);
    suite_add_tcase(s, tc_core);

    return s;
}
