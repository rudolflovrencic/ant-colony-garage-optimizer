#include <check.h>

#include "Tests.h"

/* Main function that runs the tests and reports results. */
int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    {   /* Instance loading suite tests. */
        s = instance_loading_suite();
        sr = srunner_create(s);

        srunner_run_all(sr, CK_NORMAL);
        number_failed = srunner_ntests_failed(sr);
        srunner_free(sr);
        free_testing_instace(); /* Free instance data allocated for testing. */
    }
    {   /* Parameters loading suite tests. */
        s = parameters_loading_suite();
        sr = srunner_create(s);

        srunner_run_all(sr, CK_NORMAL);
        number_failed += srunner_ntests_failed(sr);
        srunner_free(sr);
    }
    {   /* Population initialization suite tests. */
        s = population_init_suite();
        sr = srunner_create(s);

        srunner_run_all(sr, CK_NORMAL);
        number_failed += srunner_ntests_failed(sr);
        srunner_free(sr);
    }
    {   /* Fitness calculation suite tests. */
        s = fitness_calculation_suite();
        sr = srunner_create(s);

        srunner_run_all(sr, CK_NORMAL);
        number_failed += srunner_ntests_failed(sr);
        srunner_free(sr);
    }
    {   /* Sorting suite tests. */
        s = sorting_suite();
        sr = srunner_create(s);

        srunner_run_all(sr, CK_NORMAL);
        number_failed += srunner_ntests_failed(sr);
        srunner_free(sr);
    }
    {   /* Filter suite tests. */
        s = filter_suite();
        sr = srunner_create(s);

        srunner_run_all(sr, CK_NORMAL);
        number_failed += srunner_ntests_failed(sr);
        srunner_free(sr);
    }
    return (number_failed == 0) ? 0 : 1;
}
