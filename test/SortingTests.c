#include "Tests.h"

#include "Instance.h"
#include "Sorter.h"

START_TEST (simple_sorting)
{
    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");
    
    int lane1[5] = {2, 0, 1, 3, 4};    /* 345 288 333 360 276 */
    quicksort(lane1, 0, 5 - 1, &x);

    int expected[5] = {4, 0, 1, 2, 3}; /* 276 288 333 345 360*/

    ck_assert_mem_eq(lane1, expected, 5*sizeof(int));
    
    free_instance_data(&x);
}
END_TEST


Suite* sorting_suite()
{
    Suite* s = suite_create("Sorting suite");
    TCase* tc_core = tcase_create("Core");

    tcase_add_test(tc_core, simple_sorting);

    suite_add_tcase(s, tc_core);
    return s;
}
