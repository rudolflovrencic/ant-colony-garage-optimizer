# Ant Colony Garage Optimizer

[ACO](https://en.wikipedia.org/wiki/Ant_colony_optimization_algorithms)
heuristic implementation for
[Heuristic Optimization Methods course](https://www.fer.unizg.hr/en/course/hom).

## Getting started

These instructions will get you a copy of the project up and running on your 
local machine.

### Linux

You will need:
* C11 compiler
* Make

1. Clone the repository: 
`git clone https://gitlab.com/Garzet/ant-colony-garage-optimizer aco`.
2. Position inside the directory: `cd aco`.
3. Run `make` to build the project. This builds with optimizations enabled
(`-O2`).
4. Edit the configuration file (`res/parameters.xml`) to suit your needs.
5. Run the program: `./GarageOptimizer`.
6. When the program has finished, best solution is saved to `res/solution.txt`.

## Running the tests

These instructions will help you build the unit tests and run them. Note that
these instructions assume you cloned the repository and are positioned inside
the project root.

### Linux

If you want to run unit tests, besides basic prerequisites mentioned above, you
will need:
1. [Check](https://libcheck.github.io/check/) installed on your system and
`check.h` inside your compiler include path.

## Adjusting parameters

Parameters are stored in `res/parameters.xml` file so that they can be tweaked
without recompiling the program. The file has the following XML entries:
* `instance_filename` - instance of the garage optimization problem
* `population_size` - number of ants (solutions)
* `alpha` - pheromone exponent
* `beta` - heuristic knowledge exponent
* `rho` - pheromone evaporation percentage
* `max_iter` - maximum iterations (0 to disable this termination criteria)
* `max_time` - maximum time in seconds (0 to disable this termination criteria)
* `target_fitness` - maximum individual fitness termination criteria 
* `logging_frequency` - iterations between progress logging

**NOTE:** If you want to change pheromone reinforcement strategy, you will have to
modify the source code. Currently, pheromones are reinforced with fixed value
and are capped to maximum value of `1.0` (see [src/aco/Pheromones.c](https://gitlab.com/Garzet/ant-colony-garage-optimizer/blob/master/src/aco/Pheromones.c)).

### Our testing

Preliminary testing showed expected algorithm behavior and we note the following:

1. Bigger `population_size` results in more search space being explored with the
same pheromone values - better diversification. For search times around a
minute, population of `100` to a `1000` ants is recommended. For *long* search
times, we did experiments with `10000`, `50000` and even more ants. Those
runs yielded better results due to more thorough search.
2. Since pheromone and eta values are between `0` and `1`, the smaller the
exponent (`alpha`, `beta`), the more their respective knowledge governs the
search. For short search times, we recommend lower `alpha` values (e.g. `0.5`)
to aid in intensification (faster convergence), but for most of our experiments
we kept `alpha` and `beta` equal to `1.0`.
3. Evaporation should be kept fairly small and since fixed pheromone
reinforcement value is used, the algorithm is very sensitive to evaporation. 
We recommend `rho` values between `0.01` and `0.1`.

**NOTE:** `res/parameters.xml` already contains reasonable defaults of
parameters that should provide good baseline to work with and improve upon.

## Output

Example of progress logging output:
```140 (13s): [1.515314]  =>  0  2  1  3  3  1```

`140` is current iteration of the ACO algorithm, `13` is running time in
seconds, `1.515314` is best solution fitness and numbers after the `=>`
represent the solution - first number (`0`) is lane **index** picked by first
vehicle, second number (`2`) is lane picked by second vehicle, etc. Note that
lane index is lane ID reduced by one.

When one of the termination criteria is reached, the best solution is stored
into `res/solution.txt` in the decoded form - numbers in each row represent
IDs of vehicles that park in the lane with the index equal to the row index.

## License

This project is licensed under the MIT License - see the
[LICENSE.md](LICENSE.md) file for details
